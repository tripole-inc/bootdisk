# Working Linux distributions

## Background

This document provides an annotated list over Linux distributions that are
known to work with the bootdisks.

## Scope

We provide information on how to do a manual install, or adapt distribution
(raw) images, to work with the bootdisks, or more generally to boot with a grub
aware bootloader such as `Petitboot/u-root`.
A manual install can be made using QEMU or by native use of an installer iso.

We only give basic information on working distributions and minor hints on
how to get the distro up and running, and to be bootable with the bootdisks.
For detailed information the reader is referred to the adjacent README files.
The installation target for the Linux distribution is USB everywhere below
(with the bootdisk installed on SD).

The list below covers Linux distributions that have been tested and found
working on Beelink GT King Pro and Ugoos Am6 plus (both Amlogic s922x).
The list doesn't include the versions of various Linux distributions (e.g.
Ubuntu, Debian, ArchLinux) installed on the (raw) images made with our image
making scripts since these images can all be booted both with the bootdisks and
with `u-boot`.

Unless otherwise noted, everything works as intended on the distributions
listed (HDMI video, HDMI sound, Ethernet, WiFi, Bluetooth), but see the
caveats below.
Also, unless otherwise noted (and where applicable), the editions of the
distributions listed are the latest stable or long term stable (e.g. Debian
Bookworm and Ubuntu Jammy).

### Caveats

To get sound over HDMI to work (at least on s922x) it is in general required to
run a certain script once (as ordinary user).
The script is called `g12_sound.sh` and can be found e.g.
[here](https://gitlab.manjaro.org/manjaro-arm/packages/community/oc4-post-install/-/blob/master/g12_sound.sh).
Likewise, in order to get WiFi and Bluetooth up and working it is sometimes
required to locate and install the right firmware files for the chips in
question (various Internet sources describe this).

## Distributions

In general, the requirements for a Linux ARM64 image to boot and run from USB
on a TV-box/SBC is that there is a kernel and initrd file available on the
image with sufficient support for the boot medium (here USB) and for the
TV-box/SBC (SoC) in question.
For some of the Linux distributions everything is in place on the (raw) images
already as posted on the download site (or as provided by the installer), on
others only minor tweaks are needed (such as setting up a `grub.cfg` file).
In some cases a new kernel needs to be installed.

### Ubuntu

#### Ubuntu Server

Ubuntu Server ARM64 can be booted directly using the live iso (burned to USB).
Running the installer from the iso natively on a TV-box to install to disk does
not work currently (see [`README_ubuntu_inst.md`](README_ubuntu_inst.md) but
Ubuntu Server can be installed by running the installer in QEMU as described in
[`README_QEMU_inst.md`](README_QEMU_inst.md) or by using the (more cumbersome)
install method described in [`README_ubuntu_inst.md`](README_ubuntu_inst.md).

#### Ubuntu Desktop

Ubuntu Desktop ARM64 can be installed with the iso installer and QEMU as
described in [`README_QEMU_inst.md`](README_QEMU_inst.md) or with the manual
method described in [`README_ubuntu_inst.md`](README_ubuntu_inst.md).
A working Ubuntu Desktop install can also be obtained by exchanging the kernel
on a Ubuntu Raspberry Pi image, see
[`README_ubuntu_frompi.md`](README_ubuntu_frompi.md).
The Ubuntu ARM64 live iso images can be run as-is.

### Fedora

The Fedora Generic ARM64 (raw) images can be booted and run as-is.
The images can be found e.g.
[here](https://dl.fedoraproject.org/pub/fedora/linux/releases).
Fedora can also be installed with the iso installer for Fedora Server using
QEMU as described in [`README_QEMU_inst.md`](README_QEMU_inst.md).

Note: On some (older) Fedora releases the graphical login may not function
properly after package upgrades.
A simple workaround is to switch to e.g. VT3 (Ctrl-Alt-F3) and login there, and
then execute the command (as root/via sudo) `systemctl restart display-manager`.

### Debian

Debian ARM64 can be installed with the iso installer using QEMU as described in
[`README_QEMU_inst.md`](README_QEMU_inst.md) or using the debootstrap
procedure described in [`README_debian_inst.md`](README_debian_inst.md).

### openSuse

The iso installer
[images](https://download.opensuse.org/ports/aarch64/distribution/leap/15.4/iso/)
for openSuse ARM64 can be run natively on a TV-box to install openSuse.
Alternatively, a network install procedure can be used, see
[`README_distro_inst_disk.md`](README_distro_inst_disk.md).

The openSuse (raw) images for ARM64, called
[appliances](https://download.opensuse.org/ports/aarch64/distribution/leap/15.4/appliances/),
can be booted with the bootdisks as long as a new `grub.cfg` file is setup.
This file should be placed at `/boot/grub/grub.cfg` and can e.g. look like
(for edition Leap 15.4)
```
menuentry "openSuse (kernel 5.14.21-150400.24.18-default)" {

linux  /boot/Image-5.14.21-150400.24.18-default root=UUID=f77b5d5a-8d8f-4020-add2-a18f31432b10 loglevel=3 splash=silent systemd.show_status=1
initrd /boot/initrd-5.14.21-150400.24.18-default

}
```
(One can add a `devicetree` directive here also, but it is currently redundant
when booting with the bootdisks, see the READMEs for the bootdisks.)
On first boot it takes some time before the login screen appears and the root
password is "linux".

### Devuan

Devuan ARM64 can be installed using the iso installer and QEMU as described in
[`README_QEMU_inst.md`](README_QEMU_inst.md) or using the debootstrap method
described and the end of [`README_debian_inst.md`](README_debian_inst.md).

### Gentoo

The Gento iso installer for ARM64 can be booted with e.g. `petit-bootdisk` and
it contains a live CD system from which Gentoo (e.g. 17.1) can be installed.
The Gentoo handbook does not have a chapter for ARM64 but the instructions for
x86_64 can be used with some small obvious modifications. (Activating the sshd
daemon, as suggested at the live CD login, and installing via ssh is
recommended since there is a lot of copying/pasting. Remember to set the root
password before exting the chroot jail during the installation!)

For instance, by following the handbook instructions for an EFI system and
installing a precompiled version of the 6.1.38 kernel a fully functioning
Gentoo system can straightforwardly be obtained.
(This system can be booted with `Petitboot`.)

### Armbian

The Armbian Generic ARM64 (raw) images can be booted and run as-is.

The images for specific boards can be run if a simple `grub.cfg` file is added.
For instance, on Ugoos Am6 plus the image for Odroid N2 can be booted and run
with a file `/boot/grub/grub.cfg` like so (example for release 22.08.1):

```
menuentry "Armbian (kernel 5.19.5-meson64)" {

linux  /boot/vmlinuz-5.19.5-meson64 root=UUID=68fce3a0-0094-49ab-98db-1741d0916724 console=ttyAML0,115200 console=tty1 consoleblank=0
initrd /boot/initrd.img-5.19.5-meson64

}
```

Note: On some earlier releases of the Armbian Generic images (e.g. 22.08) HDMI
might not work with the default kernel (`linux-image-current-arm64`), on some
TV-boxes.
However, it is still possible to log in via ssh (root@uefi-arm64, p/w "1234")
and go through the initial setup via ssh and then install e.g. the kernel
`linux-image-current-meson64` to get back HDMI, after a reboot.

#### Ophub's builds

Ophub's [builds](https://github.com/ophub/amlogic-s9xxx-armbian) of Armbian can
be booted with a simple `grub.cfg` just like the board specific Armbian images.
However, Ophub's images already have a template for `extlinux.conf` (named
`extlinux.conf.bak`) which `u-root-bootdisk` can parse.
Therefore, it is easy to simply edit this file and use `u-root-bootdisk` for
booting.
For instance, on Ugoos Am6 plus the image for s922x can be booted with an
`/extlinux/extlinux.conf` file like so (example for Ophub build 220901):

```
label Armbian
    kernel /vmlinuz-5.10.140-ophub
    initrd /initrd.img-5.10.140-ophub
    fdt /dtb/amlogic/meson-g12b-ugoos-am6.dtb
    append root=UUID=9e7e9115-0312-4d0a-9587-8b0089cb6837 rootflags=data=writeback rw rootfstype=ext4 console=ttyAML0,115200n8 console=tty0 no_console_suspend consoleblank=0 fsck.fix=yes fsck.repair=yes net.ifnames=0 loglevel=1 voutmode=hdmi disablehpd=false overscan=100 sdrmode=auto
```

### Other distributions

As mentioned above, in general it is easy to get an ARM64 Linux distro to boot
and run with the bootdisks as long as some basic requirements are fulfilled.
When this is the case, it is mostly a matter of setting up a `grub.cfg` file,
as shown above.

Another example where a simple `grub.cfg` setup is sufficient is the
TwisterOS ArmbianV2-0-3 image for rk3399 which only requires a
`/boot/grub/grub.cfg` setup like so

```
menuentry "TwisterOS (kernel 5.10.43-rockchip64)" {

linux /boot/vmlinuz-5.10.43-rockchip64 root=UUID=f43869ab-391c-4c4b-98c0-2663c796d171 rootwait rootfstype=ext4 console=ttyAML0,115200n8 console=tty1 no_console_suspend consoleblank=0 loglevel=1
initrd /boot/initrd.img-5.10.43-rockchip64

}
```

There is also the image TwisterOS203-n2p-FINAL for OdroidN2/N2+ which likewise
can be run with a `grub.cfg` file like so

```
menuentry "TwisterOS (kernel 5.13.12-meson64)" {

linux /boot/vmlinuz-5.13.12-meson64 root=UUID=209ca851-3a74-48ed-a97c-27e3a45bcdcc rootwait rootfstype=ext4 console=ttyAML0,115200n8 console=tty1 no_console_suspend consoleblank=0 loglevel=1
initrd /boot/initrd.img-5.13.12-meson64

}
```

#### Custom

For other distributions, it can be convenient to make a custom image by going
through the following steps (as a template):

- Copy over all the files from the original image to a newly made USB with one
  single large ext4 partition (using e.g. `rsync` to retain all structure).
  (Since no bootloader is used in the MBR and gap before the partition on the
  USB, the gap before the first partition can be made mininal.)
  
- Set up a new `/boot/grub/grub.cfg` file.
  (The proper kernel command line options are dependent on the kernel used and
  the SoC.)

- Edit `/etc/fstab` to reflect the new UUID of the new rootfs on the USB.

- Sometimes needed for first boot: Copy over a kernel+initrd combo for first
  boot which is known to work for the TV-box in question (e.g. from one of
  [Ophub's kernels](https://github.com/ophub/amlogic-s9xxx-armbian)).

- Do a first boot and upgrade e.g. to the final kernel desired, and then do the
  rest of the customization from there (and update `grub.cfg` accordingly
  unless it is done automagically by an installed grub).

However, it should also be noted that if a Linux distribution image has a valid
syslinux/extlinux configuration then the `u-root-bootdisk` can in general be
used to boot the image (from USB) with minor alterations since `u-root` can
parse also such boot configurations (cf. the comments on Ophub's images above).
